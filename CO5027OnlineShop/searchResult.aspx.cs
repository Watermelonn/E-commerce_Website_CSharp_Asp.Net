﻿using PayPal.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CO5027OnlineShop
{
    public partial class searchResult : System.Web.UI.Page
    {
        string command = "SELECT* FROM[products]";

        string[] split = new string[2];
        string IDNumber;
        string Name;
        string Price;
        string Quantity;


        protected void Page_Load(object sender, EventArgs e)
        {


            string str = Request.QueryString["page"];
            string searchTerms = Request.QueryString["search"];

            if (str == "snookerCues")
            {
                lblHeaderText.Text = "Snooker Cues";
                Page.Title = "Snooker Cues - Breaking Balls";
            }
            else if (str == "poolCues")
            {
                lblHeaderText.Text = "Pool Cues";
                Page.Title = "Pool Cues - Breaking Balls";
            }
            else if (str == "tables")
            {
                lblHeaderText.Text = "Tables";
                Page.Title = "Tables - Breaking Balls";
            }
            else if (str == "Cases")
            {
                lblHeaderText.Text = "cases";
                Page.Title = "Cases - Breaking Balls";
            }
            else if (str == "chalk")
            {
                lblHeaderText.Text = "Chalk";
                Page.Title = "Chalk - Breaking Balls";
            }
            else if (str == "balls")
            {
                lblHeaderText.Text = "Balls";
                Page.Title = "Balls - Breaking Balls";
            }
            else
            {
                lblHeaderText.Text = "Search Results";
                Page.Title = "Search Results - Breaking Balls";
            }


            if (str != null)
            {

                command = command + " WHERE ([Category] = '" + str + "' )";
                repeaterDataSource.SelectCommand = command;
            }
            else
            {
                repeaterDataSource.SelectCommand = command;
            }


            if (searchTerms != null && searchTerms != "")
            {

                var splitted = searchTerms.Split(' ');

                command = command + " WHERE Name LIKE ";

                for (int x = 0; x < splitted.Length; x = x + 1)
                {
                    splitted[x] = splitted[x].Substring(1);

                    command = command + "'%" + splitted[x] + "%'";

                    if (x < splitted.Length - 1)
                    {
                        command = command + " AND Name LIKE ";
                    }

                }

                repeaterDataSource.SelectCommand = command;
                repeaterDataSource.DataBind();
                repResults.DataBind();


                Button btn = (Button)repResults.FindControl("btnAdd");


                if (btn != null)
                {
                    split = btn.CommandArgument.ToString().Split(';');

                    IDNumber = split[0];
                    Name = split[1];
                    Price = split[2];
                    Quantity = split[3];
                }



            }

        }

        protected void drpSort_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (drpSort.SelectedValue == "Relevance")
            {
                repeaterDataSource.SelectCommand = command;
                repeaterDataSource.DataBind();
                repResults.DataBind();
            }
            else if (drpSort.SelectedValue == "Price: High to Low")
            {
                repeaterDataSource.SelectCommand = command + " ORDER BY Price DESC";
                repeaterDataSource.DataBind();
                repResults.DataBind();
            }
            else if (drpSort.SelectedValue == "Price: Low to High")
            {
                repeaterDataSource.SelectCommand = command + " ORDER BY Price";
                repeaterDataSource.DataBind();
                repResults.DataBind();
            }
            else if (drpSort.SelectedValue == "Name: A to Z")
            {
                repeaterDataSource.SelectCommand = command + " ORDER BY Name";
                repeaterDataSource.DataBind();
                repResults.DataBind();
            }
        }

        protected void btnInfo_Click(object sender, EventArgs e)
        {

            Button btn = (Button)(sender);


            string str = "/viewItemDetail.aspx?ID=" + btn.CommandArgument;
            Response.Redirect(str);


        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {


            var config = ConfigManager.Instance.GetProperties();
            var accessToken = new OAuthTokenCredential(config).GetAccessToken();
            var apiContext = new APIContext(accessToken);

            Decimal subTotal = Convert.ToDecimal(Price);
            subTotal += 6;


            var product = new Item();
            product.name = Name;
            product.currency = "GBP";
            product.price = Price;
            //  product.sku = "PEPCO5027OnlineShopm15";
            product.quantity = "1";

            var transactionDetails = new Details();
            transactionDetails.tax = "0";
            transactionDetails.shipping = "6.00";
            transactionDetails.subtotal = Price;

            var transactionAmount = new Amount();
            transactionAmount.currency = "GBP";
            transactionAmount.total = subTotal.ToString();
            transactionAmount.details = transactionDetails;

            var transaction = new Transaction();
            transaction.description = "Your transaction details";
            transaction.invoice_number = Guid.NewGuid().ToString();
            transaction.amount = transactionAmount;
            transaction.item_list = new ItemList
            {
                items = new List<Item>
            { product }
            };

            var payer = new Payer();
            payer.payment_method = "PayPal";

            var redirectUrls = new RedirectUrls();
            redirectUrls.cancel_url = "http://1515295.studentwebserver.co.uk/searchResult.aspx";
            redirectUrls.return_url = "http://1515295.studentwebserver.co.uk/signOut.aspx?refer=paymentSuccess";


            var payment = Payment.Create(apiContext, new Payment
            {
                intent = "sale",
                payer = payer,
                transactions = new List<Transaction> { transaction },
                redirect_urls = redirectUrls
            });

            foreach (var link in payment.links)
            {
                if (link.rel.ToLower().Trim().Equals("approval_url"))
                {

                    Response.Redirect(link.href);
                }
            }


        }


    }
}