﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="products.aspx.cs" Inherits="CO5027OnlineShop.Admin.products" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Products</title>
    <style type="text/css">
        @font-face {
            font-family: Roboto;
            src: url('/Fonts/Roboto-Regular.ttf');
        }

        @font-face {
            font-family: Roboto Light;
            src: url('/Fonts/Roboto-Light.ttf');
        }


        body {
            font-size: 16px;
            font-family: Roboto Light;
            margin: 0 0 0 0;
            padding: 0 0 0 0;
            word-wrap: break-word;
            text-decoration: none;
        }

        .sectionHeader {
            background-color: #CE3E45;
            padding: 3px 40px 3px 40px;
            color: white;
            margin: 0 0 0 0;
            font-size: 18px;
            font-family: Roboto Light;
            font-style: normal;
        }

        .container {
            width: 1200px;
            margin-left: auto;
            margin-right: auto;
        }

        .title {
            width: 300px;
            font-family: Roboto Light;
            display: inline-flex;
        }

        .backButton {
            float: right;
            width: 300px;
            height: 50px;
            margin-top: 22px;
        }

        .table {
            display: inline-block;
            width: 100%;
            margin-top: 20px;
        }

        .textbox {
            font-family: Roboto Light;
            font-size: 22px;
            display: block;
            margin-top: 10px;
            margin-bottom: 10px;
        }

        .button {
            text-decoration: none;
            text-align: center;
            color: black;
            vertical-align: central;
            line-height: 50px;
            box-sizing: border-box;
            margin-top: 10px;
        }

        .labels {
            font-family: Roboto Light;
            font-size: 22px;
        }

        .val {
            display: block;
            color: red;
            font-family: Roboto Light;
            font-size: 22px;
        }

        .leftSection {
            display: inline-block;
            padding-top: 15px;
            width: 450px;
            margin-top: 0px;
        }

        .rightSection {
            float: right;
            display: inline-block;
            margin-top: 420px;
            width: 750px;
            text-wrap: normal;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="sectionHeader">
            <div class="container">
                <h1 class="title">Products</h1>
                <asp:Button ID="btnBack" CssClass="backButton" runat="server" Text="Back to Homepage" OnClick="btnBack_Click" BackColor="White" BorderColor="#272727" BorderStyle="Solid" BorderWidth="1px" Font-Names="Roboto Light" Font-Size="22px" />

            </div>

        </div>

        <div class="mainContent">
            <div class="container">

                <h2>Add New Products</h2>
                <div class="sectionHeader"></div>

                <asp:FormView ID="frmNewProducts" runat="server" DataKeyNames="ID" DataSourceID="db_1515295_breakingBallsFormSource" DefaultMode="Insert" RenderOuterTable="False">

                    <InsertItemTemplate>
                        <div class="leftSection">
                            <asp:Label ID="lblName" runat="server" Text="Name" CssClass="labels"></asp:Label>
                            <asp:TextBox ID="txtName" CssClass="textbox" runat="server" Text='<%# Bind("Name") %>' Width="400px" />

                            <asp:Label ID="lblDesc" runat="server" Text="Description" CssClass="labels"></asp:Label>
                            <asp:TextBox ID="txtDesc" CssClass="textbox" runat="server" TextMode="MultiLine" Style="resize: none" Text='<%# Bind("Description") %>' Rows="4" Width="400px" />

                            <asp:Label ID="lblPrice" runat="server" Text="Price" CssClass="labels"></asp:Label>
                            <asp:TextBox ID="txtPrice" CssClass="textbox" runat="server" Text='<%# Bind("Price") %>' Width="200px" />

                            <asp:Label ID="lblCategory" runat="server" Text="Category" CssClass="labels"></asp:Label>
                            <asp:DropDownList ID="drpCategory" CssClass="textbox" runat="server" Text='<%# Bind("Category") %>' Width="200px">
                                <asp:ListItem>snookerCues</asp:ListItem>
                                <asp:ListItem>poolCues</asp:ListItem>
                                <asp:ListItem>tables</asp:ListItem>
                                <asp:ListItem>cases</asp:ListItem>
                                <asp:ListItem>chalk</asp:ListItem>
                                <asp:ListItem>balls</asp:ListItem>


                            </asp:DropDownList>

                            <asp:Label ID="lblStock" runat="server" Text="Number in Stock" CssClass="labels"></asp:Label>
                            <asp:TextBox ID="txtStock" CssClass="textbox" runat="server" Text='<%# Bind("StockNumber") %>' Width="100px" />

                            <asp:Button ID="btnInsert" OnClick="btnInsert_Click" CssClass="button" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert" BackColor="White" BorderColor="#272727" BorderStyle="Solid" BorderWidth="1px" Font-Names="Roboto Light" Font-Size="22px" Height="50px" Width="100px" ValidationGroup="1" />
                            <asp:Button ID="btnClear" CssClass="button" runat="server" CausesValidation="False" CommandName="Cancel" Text="Clear" BackColor="White" BorderColor="#272727" BorderStyle="Solid" BorderWidth="1px" Font-Size="22px" Font-Names="Roboto Light" Height="50px" Width="100px" />
                        </div>
                        <div class="rightSection">

                            <asp:RequiredFieldValidator ID="valRequiredName" runat="server" ErrorMessage="Please enter a name." ControlToValidate="txtName" CssClass="val" ValidationGroup="1"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="valRequiredDesc" runat="server" ErrorMessage="Please enter a description" ControlToValidate="txtDesc" CssClass="val" ValidationGroup="1"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="valRequiredStock" runat="server" ErrorMessage="Please enter the number of stock." ControlToValidate="txtStock" CssClass="val" ValidationGroup="1"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="valRequiredPrice" runat="server" ErrorMessage="Please enter a price" ControlToValidate="txtPrice" CssClass="val" ValidationGroup="1"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="valComparePrice" runat="server" ErrorMessage="Please enter a vaild number in the format: '00.00'." Type="Currency" ControlToValidate="txtPrice" Operator="DataTypeCheck" CssClass="val" ValidationGroup="1"></asp:CompareValidator>
                            <asp:CompareValidator ID="valCompareStock" runat="server" ErrorMessage="Please enter a valid number in the stock number field." Type="Integer" ControlToValidate="txtStock" Operator="DataTypeCheck" CssClass="val" ValidationGroup="1"></asp:CompareValidator>
                        </div>

                    </InsertItemTemplate>

                </asp:FormView>

                <h2>Products Table</h2>
                <div class="sectionHeader"></div>

                <asp:GridView ID="dgvProducts" runat="server" AllowSorting="True" AutoGenerateColumns="False" AutoGenerateDeleteButton="True" AutoGenerateEditButton="True" CellPadding="4" CssClass="table" DataSourceID="db_1515295_breakingBallsSource" Font-Names="Roboto" Font-Size="16px" Font-Strikeout="False" ForeColor="#333333" GridLines="None" DataKeyNames="ID" CellSpacing="-1" BorderColor="#272727" BorderStyle="Solid" BorderWidth="1px">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:BoundField DataField="ID" HeaderText="ID" ReadOnly="True" SortExpression="ID" />
                        <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                        <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
                        <asp:BoundField DataField="Price" HeaderText="Price" SortExpression="Price" DataFormatString="£{0:0.00}" />
                        <asp:BoundField DataField="Category" HeaderText="Category" SortExpression="Category" />
                        <asp:BoundField DataField="StockNumber" HeaderText="StockNumber" SortExpression="StockNumber" />
                        <asp:HyperLinkField DataNavigateUrlFields="ID" DataNavigateUrlFormatString="upload.aspx?ID={0}" Text="Set Image" />
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>

                <asp:SqlDataSource ID="db_1515295_breakingBallsSource" runat="server" ConnectionString="<%$ ConnectionStrings:userConnectionString %>" SelectCommand="SELECT * FROM [products]" ConflictDetection="CompareAllValues" DeleteCommand="DELETE FROM [products] WHERE [ID] = @original_ID" InsertCommand="INSERT INTO [products] ([Name], [Description], [Price], [Category], [StockNumber]) VALUES (@Name, @Description, @Price, @Category, @StockNumber)" OldValuesParameterFormatString="original_{0}" UpdateCommand="UPDATE [products] SET [Name] = @Name, [Description] = @Description, [Price] = @Price, [Category] = @Category, [StockNumber] = @StockNumber WHERE [ID] = @original_ID AND [Name] = @original_Name AND [Description] = @original_Description AND [Price] = @original_Price AND [Category] = @original_Category AND [StockNumber] = @original_StockNumber">
                    <DeleteParameters>
                        <asp:Parameter Name="original_ID" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="Name" Type="String" />
                        <asp:Parameter Name="Description" Type="String" />
                        <asp:Parameter Name="Price" Type="Decimal" />
                        <asp:Parameter Name="Category" Type="String" />
                        <asp:Parameter Name="StockNumber" Type="Int32" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="Name" Type="String" />
                        <asp:Parameter Name="Description" Type="String" />
                        <asp:Parameter Name="Price" Type="Decimal" />
                        <asp:Parameter Name="Category" Type="String" />
                        <asp:Parameter Name="StockNumber" Type="Int32" />
                        <asp:Parameter Name="original_ID" Type="Int32" />
                        <asp:Parameter Name="original_Name" Type="String" />
                        <asp:Parameter Name="original_Description" Type="String" />
                        <asp:Parameter Name="original_Price" Type="Decimal" />
                        <asp:Parameter Name="original_Category" Type="String" />
                        <asp:Parameter Name="original_StockNumber" Type="Int32" />
                    </UpdateParameters>
                </asp:SqlDataSource>




                <asp:SqlDataSource ID="db_1515295_breakingBallsFormSource" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:userConnectionString %>" DeleteCommand="DELETE FROM [products] WHERE [ID] = @original_ID AND [Name] = @original_Name AND [Description] = @original_Description AND [Price] = @original_Price AND [Category] = @original_Category AND [StockNumber] = @original_StockNumber" InsertCommand="INSERT INTO [products] ([Name], [Description], [Price], [Category], [StockNumber]) VALUES (@Name, @Description, @Price, @Category, @StockNumber)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [products]" UpdateCommand="UPDATE [products] SET [Name] = @Name, [Description] = @Description, [Price] = @Price, [Category] = @Category, [StockNumber] = @StockNumber WHERE [ID] = @original_ID AND [Name] = @original_Name AND [Description] = @original_Description AND [Price] = @original_Price AND [Category] = @original_Category AND [StockNumber] = @original_StockNumber">
                    <DeleteParameters>
                        <asp:Parameter Name="original_ID" Type="Int32" />
                        <asp:Parameter Name="original_Name" Type="String" />
                        <asp:Parameter Name="original_Description" Type="String" />
                        <asp:Parameter Name="original_Price" Type="Decimal" />
                        <asp:Parameter Name="original_Category" Type="String" />
                        <asp:Parameter Name="original_StockNumber" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="Name" Type="String" />
                        <asp:Parameter Name="Description" Type="String" />
                        <asp:Parameter Name="Price" Type="Decimal" />
                        <asp:Parameter Name="Category" Type="String" />
                        <asp:Parameter Name="StockNumber" Type="Int32" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="Name" Type="String" />
                        <asp:Parameter Name="Description" Type="String" />
                        <asp:Parameter Name="Price" Type="Decimal" />
                        <asp:Parameter Name="Category" Type="String" />
                        <asp:Parameter Name="StockNumber" Type="Int32" />
                        <asp:Parameter Name="original_ID" Type="Int32" />
                        <asp:Parameter Name="original_Name" Type="String" />
                        <asp:Parameter Name="original_Description" Type="String" />
                        <asp:Parameter Name="original_Price" Type="Decimal" />
                        <asp:Parameter Name="original_Category" Type="String" />
                        <asp:Parameter Name="original_StockNumber" Type="Int32" />
                    </UpdateParameters>
                </asp:SqlDataSource>


            </div>
        </div>

    </form>
</body>
</html>
