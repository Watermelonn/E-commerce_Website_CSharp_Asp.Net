﻿<%@ Page Title="Item Details - Breaking Balls" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="viewItemDetail.aspx.cs" Inherits="CO5027OnlineShop.viewItemDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" runat="server">

    <style type="text/css">
        .leftSection {
            width: 500px;
            display: inline-block;
        }

        .mainContent {
            min-height: 835px;
        }

        .rightSection {
            width: 680px;
            display: inline-block;
            float: right;
        }

        .bottomSection {
            display: inline-block;
        }


        .imageContainer {
            margin-top: 30px;
        }

        .image {
            height: 500px;
            width: 500px;
            border: solid 1px #d5d5d5;
        }

        .name {
            font-family: Roboto Light;
            font-size: 44px;
        }

        .price {
            font-family: Roboto Light;
            font-size: 36px;
            color: #CE3E45;
            margin: 0 0 30px 0;
        }

        .stock {
            font-family: Roboto Light;
            font-size: 22px;
            margin: 10px 0 0 0;
            color: #ce3e45;
        }

        .descriptionHeading {
            font-family: Roboto Light;
            font-size: 26px;
            margin: 10px 0 10px 0;
        }

        .description {
            font-family: Roboto Light;
            font-size: 22px;
        }

        .button {
            margin: 20px 0 0 0;
            display: block;
        }

        .line {
            background-color: #d5d5d5;
            padding: 0.5px 0 0.5px 0;
            margin: 30px 0 0 0;
        }

        .quantity {
            margin-top: 30px;
        }
    </style>


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">


    <asp:FormView ID="frmProduct" runat="server" DataKeyNames="ID" DataSourceID="formViewSource">


        <ItemTemplate>

            <div class="leftSection">
                <div class="imageContainer">

                    <img src="/Images/<%# Eval("ID") %>.jpg?n=" class="image" />

                </div>
            </div>

            <div class="rightSection">

                <h1 class="name">
                    <%# Eval("Name") %>
                </h1>

                <h2 class="price">£<%# Eval("Price") %></h2>

                <div class="line"></div>
                <asp:Label ID="lblQuantity" runat="server" Text="Quantity:" Font-Names="Roboto Light" Font-Size="26px" CssClass="quantity"></asp:Label>


                <asp:TextBox ID="spnQuantity" TextMode="Number" Text="1" runat="server" min="1" max='<%# Eval("StockNumber") %>' step="1" Font-Names="Roboto Light" Font-Size="26px" BackColor="White" Width="50px" CssClass="quantity" BorderStyle="Solid" />

                <h2 class="stock">
                    <%# Eval("StockNumber") %> in Stock
                </h2>

                <asp:Button ID="btnAdd" runat="server" CommandArgument='<%# Eval("ID") + ";" + Eval("Name")+ ";" + Eval("Price")+ ";" + Eval("StockNumber") %>' Text="Buy Now" Width="200px" CssClass="button" BackColor="White" BorderStyle="Solid" Font-Names="Roboto Light" Font-Size="26px" Height="50px" OnClick="btnAdd_Click" BorderColor="#d5d5d5" BorderWidth="1.2px" />


            </div>
            <div class="bottomSection">
                <div class="line"></div>
                <h3 class="descriptionHeading">Description</h3>
                <p class="description">
                    <%# Eval("Description") %>
                </p>

            </div>

        </ItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="formViewSource" runat="server" ConnectionString="<%$ ConnectionStrings:userConnectionString %>" SelectCommand="SELECT * FROM [products] WHERE ([ID] = @ID)">
        <SelectParameters>
            <asp:QueryStringParameter Name="ID" QueryStringField="ID" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
