﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="searchResult.aspx.cs" Inherits="CO5027OnlineShop.searchResult" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" runat="server">

    <style type="text/css">
        .imageContainer {
            width: 200px;
            height: 200px;
        }

        .image {
            max-width: 200px;
            max-height: 200px;
            margin: auto 0 auto 0;
            vertical-align: middle;
            border: 1px solid #d5d5d5;
        }

        .repeaterList {
            border: 1px solid #d5d5d5;
            padding: 15px 5px 15px 5px;
            height: 200px;
            margin-top: 10px;
        }


        .imageContainer {
            float: left;
        }

        .sectionHeader {
            margin: 1% 0% 1% 0;
        }

        .heading {
            margin: 0 0 0 0;
            font-family: Roboto Light;
            font-size: 50px;
        }

        .repeaterContainer {
            margin-top: 15px;
            margin-bottom: 15px;
        }

        .mainContent {
            min-height: 835px;
        }

        .button {
            float: right;
            display: block;
            margin-left: 10px;
            cursor: pointer;
        }

        .name {
            font-family: Roboto Light;
            font-size: 28px;
            text-decoration: none;
            color: black;
            margin-top: 20px;
        }

        .nameContainer {
            height: 44%;
            width: 920px;
            float: right;
            margin-right: 20px;
        }

        .listContainer {
            height: 28%;
            width: 920px;
            float: right;
            margin-right: 20px;
        }

        .price {
            font-family: Roboto Light;
            font-size: 34px;
            text-decoration: none;
            color: #CE3E45;
            margin: 0 0 0 0;
            float: right;
            display: inline-block;
        }

        .line {
            background-color: #d5d5d5;
            padding: 0.5px 0 0.5px 0;
            margin-bottom: 10px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

    <asp:Label ID="lblHeaderText" runat="server" Text="" CssClass="heading"></asp:Label>
    <div class="sectionHeader"></div>

    <asp:Label ID="lblSort" runat="server" Text="Sort By:" Font-Names="Roboto Light" Font-Size="22px"></asp:Label>
    <asp:DropDownList ID="drpSort" runat="server" Font-Names="Roboto Light" Font-Size="22px" Width="300px" BackColor="White" AutoPostBack="True" OnSelectedIndexChanged="drpSort_SelectedIndexChanged">
        <asp:ListItem>Relevance</asp:ListItem>
        <asp:ListItem>Price: Low to High</asp:ListItem>
        <asp:ListItem>Price: High to Low</asp:ListItem>
        <asp:ListItem>Name: A to Z</asp:ListItem>
    </asp:DropDownList>

    <asp:Repeater ID="repResults" runat="server" DataSourceID="repeaterDataSource">

        <HeaderTemplate>
            <div class="repeaterContainer">
        </HeaderTemplate>
        <ItemTemplate>
            <div class="repeaterList">
                <div class="imageContainer">
                    <a href="<%# Eval("ID", "/viewItemDetail.aspx?ID={0}")%>">
                        <img src="/Images/<%# Eval("ID") %>.jpg?n=" class="image" onclick="" />
                    </a>
                </div>

                <div class="nameContainer">
                    <a class="name" href="<%# Eval("ID", "/viewItemDetail.aspx?ID={0}")%>">
                        <%# Eval("Name") %>
                    </a>

                </div>

                <div class="listContainer">

                    <p class="price">
                        £<%# Eval("Price") %>
                    </p>

                </div>

                <div class="listContainer">
                    <div class="line"></div>
                    <asp:Button ID="btnAdd" runat="server" CommandArgument='<%# Eval("ID") + ";" + Eval("Name")+ ";" + Eval("Price")+ ";" + Eval("StockNumber")%>' OnClick="btnAdd_Click" Text="Buy Now" Height="50px" Width="150px" BackColor="#CE3E45" Font-Names="Roboto Light" Font-Size="20px" BorderColor="#272727" BorderStyle="None" ForeColor="White" CssClass="button" />

                    <asp:Button ID="btnItem" CommandArgument='<%# Eval("ID")%>' OnClick="btnInfo_Click" runat="server" Text="More Info" Font-Names="Roboto Light" Font-Size="22px" Width="120px" Height="50px" BackColor="#606060" BorderColor="#272727" BorderStyle="None" ForeColor="White" CssClass="button" />

                </div>

            </div>

        </ItemTemplate>
        <FooterTemplate></div></FooterTemplate>

    </asp:Repeater>

    <asp:SqlDataSource ID="repeaterDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:userConnectionString %>"></asp:SqlDataSource>

</asp:Content>
