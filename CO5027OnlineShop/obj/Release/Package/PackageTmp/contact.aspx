﻿<%@ Page Title="Contact Us - Breaking Balls" Language="C#" MasterPageFile="Master.Master" AutoEventWireup="true" CodeBehind="contact.aspx.cs" Inherits="CO5027OnlineShop.contact" %>

<%@ Register Assembly="GoogleReCaptcha" Namespace="GoogleReCaptcha" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" runat="server">
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <style type="text/css">
        h1 {
            padding: 30px 0 0 0;
            margin: 0 0 0 0;
            font-family: Roboto Light;
            font-size: 50px;
        }

        .leftSection p, .rightSection p {
            font-family: Roboto Light;
            font-size: 22px;
        }

        .mainContent {
            height: 900px;
        }

        .sectionHeader {
            margin: 1% 0% 1% 0;
        }

        .y {
            height: 500px;
            font-size: 18px;
        }

        .contactDetails, .contactDetails div {
            width: 400px;
        }



        .house, .phone, .mail {
            width: 38px;
            margin: 8px 8px 8px 8px;
            border-radius: 50%;
        }

        .add, .num, .ema {
            display: inline-block;
            color: black;
            padding: 0 0 0 0;
            position: absolute;
            max-width: 356px;
            word-wrap: break-word;
            font-size: 18px;
        }

        .add {
            margin: 6px 0 0 0;
        }

        .num, .ema {
            margin: 16px 0 0 0;
        }

        .txtMessage {
            overflow: auto;
        }

        .name {
            margin-top: 0;
        }

        .leftSection {
            float: left;
            padding-top: 15px;
            margin-right: 5%;
            width: 600px;
        }

        .rightSection {
            display: inline-flex;
            padding-top: 15px;
            width: 540px;
        }

        #map {
            width: 600px;
            height: 500px;
            margin-top: 15px;
        }

        .btnSubmit {
            display: block;
            margin-bottom: 15px;
        }

        .g-recaptcha {
            margin-top: 10px;
        }

        .map {
            width: 600px;
            margin-top: 20px;
            float: left;
        }
    </style>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

    <h1>Contact Us</h1>

    <div class="sectionHeader">
    </div>

    <div class="leftSection">
        <div class="contactDetails">
            <div>
                <img src="/Images/house.png" class="house" />
                <p class="add">72b Canal Road, Chester Business Park, Chester, Cheshire CH1 6ZZ</p>
            </div>
            <div>
                <img src="/Images/phone.png" class="phone" />
                <p class="num">+44 01244 123456</p>
            </div>
            <div>
                <img src="/Images/mail.png" class="mail" />
                <p class="ema">enquiries@breakingballs.com</p>
            </div>

        </div>



        <noscript>
            <img src="/Images/map.png" class="map" alt="Our business located on the map of the local area"/>
        </noscript>

        <div id="map">
            <script>
                var map;
                function initMap() {
                    map = new google.maps.Map(document.getElementById('map'), {
                        center: { lat: 53.161624, lng: -2.898900 },
                        zoom: 15

                    });

                    var marker = new google.maps.Marker({
                        position: { lat: 53.161624, lng: -2.898900 },
                        map: map,
                        title: 'Breaking Balls Co'
                    });

                }
            </script>


            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCtQfsIOiCNCMee9yT4c-QIgQE0FwGEPQw&callback=initMap" async defer></script>

        </div>

    </div>

    <div class="rightSection">

        <div class="contactForm">
            <p class="name">Name</p>
            <asp:TextBox ID="txtName" CssClass="txtName" runat="server" BorderStyle="Solid" BorderWidth="1px" BorderColor="#272727" BackColor="Transparent" Font-Size="22px" Font-Names="Roboto Light" Style="width: 400px;"></asp:TextBox>

            <p class="emailAddress">Email Address</p>
            <asp:TextBox ID="txtAddress" CssClass="txtAddress" runat="server" TextMode="Email" BorderStyle="Solid" BorderWidth="1px" BorderColor="#272727" BackColor="Transparent" Font-Size="22px" Font-Names="Roboto Light" Style="width: 400px;" ToolTip="Please enter a valid email address."></asp:TextBox>
            <p class="message">Message</p>
            <asp:TextBox ID="txtMessage" CssClass="txtMessage" runat="server" TextMode="MultiLine" BorderStyle="Solid" BorderWidth="1px" BorderColor="#272727" BackColor="Transparent" Font-Size="22px" Font-Names="Roboto Light" Style="resize: none; width: 400px; height: 100px;"></asp:TextBox>
            <div class="g-recaptcha" data-sitekey="6LfiC28UAAAAAJUQ5J_wHhqyFza8TCAwg0D8UJJq"></div>
            <asp:Button ID="btnSubmit" CssClass="btnSubmit" runat="server" Text="Submit" BorderColor="#272727" BorderStyle="Solid" BorderWidth="1px" Font-Size="22px" Font-Names="Roboto Light" BackColor="Transparent" Style="margin-top: 10px; width: 100px; height: 50px; font-size: 18px;" OnClick="btnSubmit_Click" />
            <asp:Label ID="lblCaptcha" runat="server" Text="" ForeColor="Red"></asp:Label>
            <br />
            <asp:RequiredFieldValidator ID="VALtxtName" runat="server" ErrorMessage="*The Name field cannot be blank. Please enter a vaild Name." ControlToValidate="txtName" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
            <br />
            <asp:RequiredFieldValidator ID="VALtxtAddress" runat="server" ErrorMessage="*The Email Address field cannot be blank. Please enter a valid Email Address." ControlToValidate="txtAddress" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
            <br />
            <asp:RequiredFieldValidator ID="VALtxtMessage" runat="server" ErrorMessage="*The Message field cannot be blank. Please enter a valid Message." ControlToValidate="txtMessage" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
        </div>

    </div>


</asp:Content>
