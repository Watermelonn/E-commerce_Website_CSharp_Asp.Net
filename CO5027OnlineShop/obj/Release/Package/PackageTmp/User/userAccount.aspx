﻿<%@ Page Title="My Account - Breaking Balls" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="userAccount.aspx.cs" Inherits="CO5027OnlineShop.User.userAccount" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" runat="server">

     <style type="text/css">
         @font-face {
            font-family: Roboto;
            src: url('../Fonts/Roboto-Regular.ttf');
        }

        @font-face {
            font-family: Roboto Light;
            src: url('../Fonts/Roboto-Light.ttf');
        }


        h1 {
            padding: 30px 0 0 0;
            margin: 0 0 0 0;
            font-family: Roboto Light;
            font-size: 50px;
        }

        .sectionHeader {
            margin: 1% 0% 1% 0;
        }

        .mainContent {
            min-height: 835px;
        }

        .button {
            display: block;
        }

        .label {
             display: block;
        }

        .textbox {
             display: block;
             margin-top: 15px;
        }

        .val{
             display: block;
             color:red;
        }

        .heading{
            margin-bottom:20px;
        }

    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

    <h1>My Account</h1>
    <div class="sectionHeader">
    </div>


    <asp:Label ID="lblChangePassword" runat="server" Text="Change your password:" Font-Names="Roboto Light" Font-Size="24px" CssClass="label heading"></asp:Label>

    <asp:Label ID="lblExistingPassword" runat="server" Text="Existing Password" Font-Names="Roboto Light" Font-Size="22px" CssClass="label"></asp:Label>
    <asp:TextBox ID="txtExistingPassword" runat="server" Font-Names="Roboto Light" Font-Size="22px" Width="300px" TextMode="Password" CssClass="textbox"></asp:TextBox>
    <asp:RequiredFieldValidator ID="valRequiredExistingPassword" runat="server" ControlToValidate="txtExistingPassword" ErrorMessage="Please enter your existing password!" Font-Names="Roboto Light" Font-Size="22px" CssClass="val" ValidationGroup="val"></asp:RequiredFieldValidator>

    <asp:Label ID="lblNewPassword" runat="server" Text="New Password" Font-Names="Roboto Light" Font-Size="22px" CssClass="label"></asp:Label>
    <asp:TextBox ID="txtNewPassword" runat="server" Font-Names="Roboto Light" Font-Size="22px" Width="300px" TextMode="Password" CssClass="textbox"></asp:TextBox>
    <asp:RequiredFieldValidator ID="valRequiredNewPassword" runat="server" ControlToValidate="txtNewPassword" ErrorMessage="Please enter a new password!" Font-Names="Roboto Light" Font-Size="22px" CssClass="val" ValidationGroup="val"></asp:RequiredFieldValidator>

    <asp:Label ID="lblConfirmPassword" runat="server" Text="Confirm New Password" Font-Names="Roboto Light" Font-Size="22px" CssClass="label"></asp:Label>
    <asp:TextBox ID="txtConfirmPassword" runat="server" Font-Names="Roboto Light" Font-Size="22px" Width="300px" TextMode="Password" CssClass="textbox"></asp:TextBox>
    <asp:RequiredFieldValidator ID="valRequiredConfirmPassword" runat="server" ControlToValidate="txtConfirmPassword" ErrorMessage="Please re-enter your password in the Confirm Password field!" Font-Names="Roboto Light" Font-Size="22px" CssClass="val" ValidationGroup="val"></asp:RequiredFieldValidator>

    <asp:Button ID="btnNewPassword" runat="server" Text="Change" Font-Names="Roboto Light" Font-Size="22px" BorderColor="#272727" BorderStyle="Solid" BorderWidth="1px" BackColor="White" Height="50px" Width="100px" cssclass="button" ValidationGroup="val" OnClick="btnNewPassword_Click"/>
    <asp:CompareValidator ID="valComparePassword" runat="server" ErrorMessage="Your new password does not match! Please check your spelling and re-enter your new password in the New Password and Confirm Password fields." Font-Names="Roboto Light" Font-Size="22px" ControlToValidate="txtNewPassword" ControlToCompare="txtConfirmPassword" CssClass="val" ValidationGroup="val"></asp:CompareValidator>
    <asp:Label ID="lblValidation" runat="server" Text="" Font-Names="Roboto Light" Font-Size="22px" CssClass="val"></asp:Label>
</asp:Content>
