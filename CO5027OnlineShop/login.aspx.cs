﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;

namespace CO5027OnlineShop
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {


            var identityDbContext = new IdentityDbContext("userConnectionString");
            var userStore = new UserStore<IdentityUser>(identityDbContext);
            var userManager = new UserManager<IdentityUser>(userStore);


            var user = new IdentityUser() { UserName = txtCreateUsername.Text, Email = txtEmail.Text };
            IdentityResult result = userManager.Create(user, txtCreatePassword.Text);
            if (result.Succeeded)
            {
                var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
                var userIdentity = userManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
                authenticationManager.SignIn(new AuthenticationProperties() { }, userIdentity);

                userManager.AddToRole(user.Id, "User");
                userManager.Update(user);

                lblCreateValidation.Text = "Account created, you can now log in.";

            }
            else
            {
                lblCreateValidation.Text = "An error has occurred: " + result.Errors.FirstOrDefault();
            }


        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            var identityDbContext = new IdentityDbContext("userConnectionString");
            var userStore = new UserStore<IdentityUser>(identityDbContext);
            var userManager = new UserManager<IdentityUser>(userStore);
            var user = userManager.Find(txtUsername.Text, txtPassword.Text);
            if (user != null)
            {
                LogUserIn(userManager, user);
            }
            else
            {
                lblValidation.Text = "Invalid username or password.";
            }
        }

        private void LogUserIn(UserManager<IdentityUser> userManager, IdentityUser user)
        {
            var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
            var userIdentity = userManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
            authenticationManager.SignIn(new AuthenticationProperties() { }, userIdentity);

            var admin = userManager.IsInRole(user.Id, "Admin");

            if (admin)
            {
                Response.Redirect("/Admin/products.aspx");
            }

        }
    }
}
