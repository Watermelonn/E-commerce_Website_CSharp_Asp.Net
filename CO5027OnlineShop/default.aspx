﻿<%@ Page Title="Breaking Balls - The UK's Premier Billiards Stockists" Language="C#" MasterPageFile="Master.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="CO5027OnlineShop._default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" runat="server">
    <style type="text/css">
        .banner {
            padding-top: 5%;
            width: 100%;
        }

        .offer01, .offer02, .offer03 {
            border: solid 6px #272727;
            width: 31.44%;
        }

        .offer01, .offer02 {
            margin-right: 1%;
        }

        .table {
            width: 100%;
        }

        .description {
            border: 4px solid #272727;
            padding: 18px 18px 18px 18px;
        }

        .brands {
            width: 60%;
            margin-left: 20%;
            margin-bottom: 4%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <img src="/Images/snooker.jpg" class="banner" alt="Breaking Balls, the UK's number one stockist of pool and snooker equipment" />
    <div class="sectionHeader">
        <p>NEW ARRIVALS</p>
    </div>
    <a href="/searchResult.aspx?search=Montfort">
        <img src="/Images/table.jpg" class="table" alt="Montfort Pool Tables now available to purchase" />
    </a>

    <div class="sectionHeader">

        <p>NEW PRODUCTS</p>
    </div>
    <a href="/viewItemDetail.aspx?ID=18">

        <img src="/Images/offer_01.jpg" class="offer01" alt="Hustler 7 Inch Pool Table for only £479.99" />
    </a>
    <a href="/viewItemDetail.aspx?ID=6">

        <img src="/Images/offer_02.jpg" class="offer02" alt="Cannon Diamond 3/4 Snooker Cue for only £64.00" />
    </a>
    <a href="/viewItemDetail.aspx?ID=13">
        <img src="/Images/offer_03.jpg" class="offer03" alt="2 1/4 Inch Competition American Pool Ball Set only £19.99" />
    </a>

    <div class="sectionHeader"></div>
    <p class="description">Welcome to Breaking Balls, the UK’s no. 1 snooker and pool retailer. Stocking a huge range of billiards equipment for players of all levels, Breaking Balls has all of your snooker and pool needs covered. Our snooker and pool equipment provide fantastic value for money, choose from a wide range of major brands online and in store including Peradon, Riley and Brittania. </p>
    <div class="sectionHeader"></div>
    <img src="/Images/Brands.png" class="brands" alt="We stock all major brands" />
</asp:Content>
