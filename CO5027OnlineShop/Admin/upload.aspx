﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="upload.aspx.cs" Inherits="CO5027OnlineShop.Admin.upload" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Upload</title>
    <style type="text/css">
        @font-face {
            font-family: Roboto;
            src: url('/Fonts/Roboto-Regular.ttf');
        }

        @font-face {
            font-family: Roboto Light;
            src: url('/Fonts/Roboto-Light.ttf');
        }


        body {
            font-size: 16px;
            font-family: Roboto Light;
            margin: 0 0 0 0;
            padding: 0 0 0 0;
            word-wrap: break-word;
            text-decoration: none;
        }

        .sectionHeader {
            background-color: #CE3E45;
            padding: 3px 40px 3px 40px;
            color: white;
            margin: 0 0 0 0;
            font-size: 18px;
            font-family: Roboto Light;
            font-style: normal;
        }

        .container {
            width: 1200px;
            margin-left: auto;
            margin-right: auto;
        }

        .title {
            width: 300px;
            font-family: Roboto Light;
            display: inline-flex;
        }

        .backButton {
            float: right;
            width: 300px;
            height: 50px;
            margin-top: 22px;
        }

        .mainSection {
            margin-top: 15px;
        }

        .image {
            display: inline-block;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:FormView ID="frmUpload" runat="server" DataKeyNames="ID" DataSourceID="formViewSource">
            <ItemTemplate>
                <div class="sectionHeader">
                    <div class="container">
                        <h1>
                            <%# Eval("Name") %>
                        </h1>

                        <asp:Button ID="btnBack" CssClass="backButton" runat="server" Text="Back to Products Page" OnClick="btnBack_Click" BackColor="White" BorderColor="#272727" BorderStyle="Solid" BorderWidth="1px" Font-Names="Roboto Light" Font-Size="22px" Font-Bold="False" />

                    </div>
                </div>

                <div class="mainContent">
                    <div class="container">
                        <div class="mainSection">
                            <asp:FileUpload ID="uplImage" runat="server" accept=".jpg" BackColor="White" BorderColor="#272727" BorderStyle="Solid" BorderWidth="1px" Font-Names="Roboto Light" Font-Size="22px" Width="400px" />
                            <asp:Button ID="btnImage" runat="server" Text="Upload" BackColor="White" BorderColor="#272727" BorderStyle="Solid" BorderWidth="1px" Font-Names="Roboto Light" Font-Size="22px" Font-Underline="False" Height="50px" OnClick="btnImage_Click" Width="100px" />
                            <asp:Label ID="lblFileType" runat="server" Text=""></asp:Label>
                            <asp:Image ID="image" runat="server" ImageAlign="Middle" Width="500px" CssClass="image" />

                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:FormView>
        <asp:SqlDataSource ID="formViewSource" runat="server" ConnectionString="<%$ ConnectionStrings:userConnectionString %>" SelectCommand="SELECT * FROM [products] WHERE ([ID] = @ID)">
            <SelectParameters>
                <asp:QueryStringParameter Name="ID" QueryStringField="ID" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
    </form>
</body>
</html>
