﻿using System;
using System.IO;
using System.Web.UI.WebControls;

namespace CO5027OnlineShop.Admin
{
    public partial class upload : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string ID = Request.QueryString["ID"];
            string filename = ID + ".jpg";

            if(ID == null)
            {
                Response.Redirect("/Admin/products.aspx");
            }
            else
            {
                Image image = (Image)frmUpload.FindControl("image");
                image.ImageUrl = "~/Images/" + filename + "?n=" + DateTime.Now.Second.ToString();

                Console.WriteLine(image.ImageUrl);
            }
            

        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Admin/products.aspx");
        }

        protected void btnImage_Click(object sender, EventArgs e)
        {


            string ID = Request.QueryString["ID"];
            string filename = ID + ".jpg";
            string saveLocation = Server.MapPath("~/Images/" + filename);

            FileUpload uplImage = (FileUpload)frmUpload.FindControl("uplImage");
            Label lblFileType = (Label)frmUpload.FindControl("lblFileType");

            string fileExtension = System.IO.Path.GetExtension(uplImage.FileName);

            File.Delete(saveLocation);

            if (uplImage.HasFile)
            {

                if (fileExtension == ".jpg" || fileExtension == ".jpeg")
                {
                    uplImage.SaveAs(saveLocation);
                }
                else
                {
                    lblFileType.Text = "Only JPEG files accepted!";
                }

            }
        }
    }
}