﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;

namespace CO5027OnlineShop
{
    public partial class signOut : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string refer = Request.QueryString["refer"];

            if (refer == "paymentSuccess")
            {
                lblSuccess.Text = "Order completed. Expect to recieve your order in around 2-4 years!";
            }

            else
            {
                lblSuccess.Text = "You have been successfully signed out!";
            }


        }
    }
}