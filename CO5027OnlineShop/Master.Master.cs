﻿using System;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CO5027OnlineShop
{
    public partial class Master : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
            {


                var userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(new IdentityDbContext("userConnectionString")));
                var user = userManager.FindByName(Context.User.Identity.GetUserName());
                var admin = userManager.IsInRole(user.Id, "Admin");

                if (admin)
                {
                    lblAccount.NavigateUrl = "/Admin/products.aspx";
                }
                else
                {
                    lblAccount.NavigateUrl = "/User/userAccount.aspx";
                }


                lblCurrentLoggedInUser.Text = System.Web.HttpContext.Current.User.Identity.Name;

                btnSignOut.ForeColor = System.Drawing.ColorTranslator.FromHtml("#CE3E45");
                btnSignOut.Text = " (SIGN OUT)";
                btnSignOut.Enabled = true;

            }
            else
            {
                lblAccount.NavigateUrl = "/User/userAccount.aspx";
            }


        }

        protected void btnSignOut_Click(object sender, EventArgs e)
        {
            if (btnSignOut.Text == "LOGIN")
            {

                Response.Redirect("/User/userAccount.aspx");

            }
            else
            {
                HttpContext.Current.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                Response.Redirect("/signOut.aspx");
            }

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {

            Response.Redirect("/searchResult.aspx?search=" + txtSearch.Text);

        }


    }
}