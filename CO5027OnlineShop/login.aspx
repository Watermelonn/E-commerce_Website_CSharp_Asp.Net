﻿<%@ Page Title="Log In - Breaking Balls" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="CO5027OnlineShop.login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" runat="server">

    <style type="text/css">
        h1 {
            padding: 30px 0 0 0;
            margin: 0 0 0 0;
            font-family: Roboto Light;
            font-size: 50px;
        }

        .sectionHeader {
            margin: 1% 0% 1% 0;
        }

        .leftSection {
            float: left;
            padding-top: 15px;
            width: 500px;
            margin-top: 0px;
        }

        .rightSection {
            display: inline-block;
            padding-top: 15px;
            width: 300px;
            text-wrap: normal;
        }

        .loginTextBox {
            margin-top: 10px;
            font-size: 22px;
        }

        .loginLabel {
            margin-top: 10px;
        }


        .loginButton {
            margin-top: 10px;
            font-family: Roboto Light;
            font-size: 18px;
        }

        .mainContent {
            height: 835px;
        }

        .textBoxes {
            margin-top: 10px;
            width: 400px;
        }

        .labels {
            display: block;
            margin-top: 10px;
        }

        .buttons {
            margin-top: 20px;
            display: block;
        }

        .val {
            display: block;
            font-size: 22px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

    <h1>Login</h1>
    <div class="sectionHeader">
    </div>


    <asp:LoginView ID="loginView" runat="server"></asp:LoginView>


    <div class="leftSection">

        <asp:Label ID="lblUsername" runat="server" Text="Username" Font-Names="Roboto Light" Font-Size="22px" CssClass="labels"></asp:Label>
        <asp:TextBox ID="txtUsername" runat="server" BackColor="White" BorderColor="#272727" BorderStyle="Solid" BorderWidth="1px" Font-Names="Roboto" Font-Size="22px" AutoCompleteType="None" CausesValidation="True" CssClass="textBoxes" Width="300px" MaxLength="20"></asp:TextBox>
        <asp:Label ID="lblPassword" runat="server" Text="Password" Font-Names="Roboto Light" Font-Size="22px" CssClass="labels"></asp:Label>
        <asp:TextBox ID="txtPassword" runat="server" BackColor="White" BorderColor="#272727" BorderStyle="Solid" BorderWidth="1px" Font-Names="Roboto" Font-Size="22px" AutoCompleteType="None" CausesValidation="True" TextMode="Password" CssClass="textBoxes" Width="300px"></asp:TextBox>
        <asp:Button ID="btnLogin" runat="server" Text="Login" Font-Names="Roboto Light" BackColor="White" BorderStyle="Solid" BorderColor="#272727" BorderWidth="1px" Font-Size="18px" OnClick="btnLogin_Click" CssClass="buttons" Height="50px" Width="100px" />
        <asp:Label ID="lblValidation" runat="server" Text="" ForeColor="Red" CssClass="val"></asp:Label>
    </div>

    <div class="rightSection">
        <asp:Label ID="lblTitle" runat="server" Text="Don't have an account? Create one here:" Font-Names="Roboto Light" Font-Size="22px" CssClass="labels" Font-Bold="True"></asp:Label>

        <asp:Label ID="labelsUsername" runat="server" Text="Username" Font-Names="Roboto Light" Font-Size="22px" CssClass="labels"></asp:Label>
        <asp:TextBox ID="txtCreateUsername" ToolTip="Username" runat="server" BackColor="White" BorderColor="#272727" BorderStyle="Solid" BorderWidth="1px" Font-Names="Roboto" Font-Size="22px" AutoCompleteType="None" CausesValidation="True" MaxLength="20" CssClass="textBoxes"></asp:TextBox>
        <asp:Label ID="labelsPassword" runat="server" Text="Password" Font-Names="Roboto Light" Font-Size="22px" CssClass="labels"></asp:Label>
        <asp:TextBox ID="txtCreatePassword" ToolTip="Password" runat="server" TextMode="Password" BackColor="White" BorderColor="#272727" BorderStyle="Solid" BorderWidth="1px" Font-Names="Roboto" Font-Size="22px" AutoCompleteType="None" CausesValidation="True" CssClass="textBoxes"></asp:TextBox>
        <asp:Label ID="lblConfirmPassword" runat="server" Text="Confirm Password" Font-Names="Roboto Light" Font-Size="22px" CssClass="labels"></asp:Label>
        <asp:TextBox ID="txtConfirmPassword" ToolTip="Confirm Password" runat="server" BackColor="White" BorderColor="#272727" BorderStyle="Solid" BorderWidth="1px" Font-Names="Roboto" Font-Size="22px" AutoCompleteType="None" CausesValidation="True" TextMode="Password" CssClass="textBoxes"></asp:TextBox>
        <asp:Label ID="lblEmail" runat="server" Text="Email Address" Font-Names="Roboto Light" Font-Size="22px" CssClass="labels"></asp:Label>
        <asp:TextBox ID="txtEmail" ToolTip="Email" runat="server" BackColor="White" BorderColor="#272727" BorderStyle="Solid" BorderWidth="1px" Font-Names="Roboto" Font-Size="22px" AutoCompleteType="None" CausesValidation="True" TextMode="Email" CssClass="textBoxes"></asp:TextBox>
        <asp:Button ID="btnSubmit" runat="server" Text="Submit" Font-Names="Roboto Light" BackColor="White" BorderStyle="Solid" BorderColor="#272727" BorderWidth="1px" Font-Size="18px" OnClick="btnSubmit_Click" CssClass="buttons" Height="50px" Width="100px" ValidationGroup="val1" />

        <asp:RequiredFieldValidator ID="VALtxtUsername" runat="server" ErrorMessage="Please enter a username." ControlToValidate="txtCreateUsername" Display="Dynamic" ForeColor="Red" CssClass="val" ValidationGroup="val1"></asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="VALtxtPassword" runat="server" ErrorMessage="Please enter a password." ControlToValidate="txtCreatePassword" Display="Dynamic" ForeColor="Red" CssClass="val" ValidationGroup="val1"></asp:RequiredFieldValidator>
        <asp:CompareValidator ID="VALtxtPasswordCOMPtxtConfirmPassword" runat="server" ErrorMessage="Passwords do not match" ControlToValidate="txtPassword" Display="Dynamic" ForeColor="Red" ControlToCompare="txtConfirmPassword" CssClass="val" ValidationGroup="val1"></asp:CompareValidator>
        <asp:RequiredFieldValidator ID="VALtxtEmail" runat="server" ErrorMessage="Please enter your email address." ControlToValidate="txtEmail" Display="Dynamic" ForeColor="Red" CssClass="val" ValidationGroup="val1"></asp:RequiredFieldValidator>
        <asp:Label ID="lblCreateValidation" runat="server" Text="" ForeColor="Red"></asp:Label>

    </div>




</asp:Content>
