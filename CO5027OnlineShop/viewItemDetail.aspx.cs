﻿using PayPal;
using PayPal.Api;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CO5027OnlineShop
{
    public partial class viewItemDetail : System.Web.UI.Page
    {

        string[] str = new string[2];
        string IDNumber;
        string Name;
        string Price;
        string Quantity;

        protected void Page_Load(object sender, EventArgs e)
        {
                frmProduct.FindControl("btnAdd");

            
            //if (btn != null)
            //{
            //    str = btn.CommandArgument.ToString().Split(';');

            //    IDNumber = str[0];
            //    Name = str[1];
            //    Price = str[2];
            //    Quantity = str[3];
            //}


            //if (Quantity != null)
            //{

            //    if (Quantity == "0")
            //    {
            //        TextBox spnQuantity = (TextBox)frmProduct.FindControl("spnQuantity");

            //        spnQuantity.Text = "0";
            //        spnQuantity.TextMode = TextBoxMode.SingleLine;
            //        spnQuantity.ReadOnly = true;


            //        btn.Enabled = false;
            //        btn.Text = "Out of Stock";
            //    }
            //}

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {



            TextBox spnQuantity = (TextBox)frmProduct.FindControl("spnQuantity");
            var config = ConfigManager.Instance.GetProperties();
            var accessToken = new OAuthTokenCredential(config).GetAccessToken();
            var apiContext = new APIContext(accessToken);

            int quantityToInt = Int32.Parse(spnQuantity.Text);
            Decimal Total = Convert.ToDecimal(Price);

            Total = Total * quantityToInt;




            var product = new Item();
            product.name = Name;
            product.currency = "GBP";
            product.price = Price;
            //  product.sku = "PEPCO5027OnlineShopm15";
            product.quantity = spnQuantity.Text;

            var transactionDetails = new Details();
            transactionDetails.tax = "0";
            transactionDetails.shipping = "6.00";
            transactionDetails.subtotal = Total.ToString();

            Total += 6;

            var transactionAmount = new Amount();
            transactionAmount.currency = "GBP";
            transactionAmount.total = Total.ToString();
            transactionAmount.details = transactionDetails;

            var transaction = new Transaction();
            transaction.description = "Your transaction details";
            transaction.invoice_number = Guid.NewGuid().ToString();
            transaction.amount = transactionAmount;
            transaction.item_list = new ItemList
            {
                items = new List<Item>
            { product }
            };

            var payer = new Payer();
            payer.payment_method = "PayPal";

            var redirectUrls = new RedirectUrls();
            redirectUrls.cancel_url = "http://1515295.studentwebserver.co.uk/searchResult.aspx";
            redirectUrls.return_url = "http://1515295.studentwebserver.co.uk/signOut.aspx?refer=paymentSuccess";

            var payment = Payment.Create(apiContext, new Payment
            {
                intent = "sale",
                payer = payer,
                transactions = new List<Transaction> { transaction },
                redirect_urls = redirectUrls
            });

            foreach (var link in payment.links)
            {
                if (link.rel.ToLower().Trim().Equals("approval_url"))
                {

                    Response.Redirect(link.href);
                }
            }

        }

    }
}