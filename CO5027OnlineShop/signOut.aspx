﻿<%@ Page Title="Signed Out - Breaking Balls" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="signOut.aspx.cs" Inherits="CO5027OnlineShop.signOut" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" runat="server">

    <style type="text/css">
        h1 {
            padding: 30px 0 0 0;
            margin: 0 0 0 0;
            font-family: Roboto Light;
            font-size: 50px;
        }

        .sectionHeader {
            margin: 1% 0% 1% 0;
        }

        .mainContent {
            min-height: 835px;
        }

        .success {
            margin: 30px 0 50px 0;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">

    <h1>Success!</h1>

    <div class="sectionHeader"></div>
    <div class="mainSection">

        <asp:Label ID="lblSuccess" CssClass="success" runat="server" Text="" Font-Names="Roboto Light" Font-Size="22px"></asp:Label>


    </div>
</asp:Content>
