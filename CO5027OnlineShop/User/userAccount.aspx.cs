﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CO5027OnlineShop.User
{
    public partial class userAccount : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            

            
        }

        protected void btnNewPassword_Click(object sender, EventArgs e)
        {
            var identityDbContext = new IdentityDbContext("userConnectionString");
            var userStore = new UserStore<IdentityUser>(identityDbContext);
            var userManager = new UserManager<IdentityUser>(userStore);

            string str = System.Web.HttpContext.Current.User.Identity.GetUserName();

            var user = userManager.Find(str, txtExistingPassword.Text);


            if (user != null)
            {

                userManager.RemovePassword(user.Id);

                userManager.AddPassword(user.Id, txtNewPassword.Text);

                lblValidation.Text = "Password changed!";

            }
            else
            {
                lblValidation.Text = "The existing password you entered is incorrect! Please try again.";
            }

        }
    }
}